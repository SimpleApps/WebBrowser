from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtWebEngineWidgets import *
from PyQt5.QtPrintSupport import *
from PyQt5.QtWebEngineCore import QWebEngineUrlRequestInterceptor, QWebEngineUrlSchemeHandler, QWebEngineUrlScheme
from PyQt5 import QtCore, QtWidgets, QtWebEngineCore, QtWebEngineWidgets

import urllib.parse
import os
import sys
import json
import validators
import feedparser
import random
from functools import partial
import webbrowser
import toml
import pathlib
import re
import tkinter as tk
from tkinter import ttk
from tkinter import *

version = "0.01"
is_64bits = sys.maxsize > 2**32

def show_settings():
    with open('config.toml') as f:
        configfile = f.read()

    root = tk.Tk()
    root.title('Settings')

    window_width = 1000
    window_height = 800

    # get the screen dimension
    screen_width = root.winfo_screenwidth()
    screen_height = root.winfo_screenheight()

    # find the center point
    center_x = int(screen_width/2 - window_width / 2)
    center_y = int(screen_height/2 - window_height / 2)

    # set the position of the window to the center of the screen
    root.geometry(f'{window_width}x{window_height}+{center_x}+{center_y}')

    label = ttk.Label(text = "Settings").pack(side="top")

    def saveInput():
        inp = inputtxt.get(1.0, "end-1c")
        with open("config.toml", "w") as f:
            f.write(inp)

    # TextBox Creation
    inputtxt = tk.Text(root, height = 45, width = window_width)

    inputtxt.pack()

    inputtxt.insert(END, configfile)

    # Button Creation
    printButton = tk.Button(root, text = "Save Settings", command = saveInput)
    printButton.pack()

    label = ttk.Label(text = "You will have to restart Web Browser for the changes to take effect.").pack(side="top")

    root.mainloop()

class AboutDialog(QDialog):
    def __init__(self, *args, **kwargs):
        super(AboutDialog, self).__init__(*args, **kwargs)

        QBtn = QDialogButtonBox.Ok  # No cancel
        self.buttonBox = QDialogButtonBox(QBtn)
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)

        layout = QVBoxLayout()

        title = QLabel("Web Browser")
        font = title.font()
        font.setPointSize(20)
        title.setFont(font)

        layout.addWidget(title)

        logo = QLabel()
        logo.setPixmap(QPixmap(os.path.join(web_browser_path, 'images', 'web_browser-icon-64.png')))
        layout.addWidget(logo)

        layout.addWidget(QLabel("Version " + version))

        for i in range(0, layout.count()):
            layout.itemAt(i).setAlignment(Qt.AlignHCenter)

        layout.addWidget(self.buttonBox)

        self.setLayout(layout)

class MainWindow(QMainWindow):
    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)

        global profile_name

        profile_name = "Default Profile"

        self.tabs = QTabWidget()
        self.tabs.setDocumentMode(True)
        self.tabs.tabBarDoubleClicked.connect(self.tab_open_doubleclick)
        self.tabs.currentChanged.connect(self.current_tab_changed)
        self.tabs.setTabsClosable(True)
        self.tabs.tabCloseRequested.connect(self.close_current_tab)

        self.tabs.setStyleSheet("""
                        QTabWidget {
                            background: #ffffff;
                        }
                        QTabBar {
                            background: #e7eaed;
                        }
                        QTabBar::tab {
                            background: #e7eaed;
                            padding: 7px;
                            color: #000000;
                            margin-top: -1px;
                            margin-bottom: -1px;
                            margin-left: 1pt solid black;
                            margin-right: 1pt solid black;
                            border: 1px solid #000000;
                            border-radius: 4px;
                            width: 200px;
                        }
                        QTabBar::tab:hover {
                            background: #f0f0f0;
                        }
                        QTabBar::tab:selected {
                            background: #ffffff;
                            color: #000000;
                        }
                        """)

        self.setCentralWidget(self.tabs)

        self.status = QStatusBar()
        self.setStatusBar(self.status)

        navtb = QToolBar("Navigation")
        navtb.setIconSize(QSize(16, 16))
        self.addToolBar(navtb)

        back_btn = QAction(QIcon(os.path.join(web_browser_path, 'images', 'arrow-180.png')), "Back", self)
        back_btn.setStatusTip("Back to previous page")
        back_btn.triggered.connect(lambda: self.tabs.currentWidget().back())
        navtb.addAction(back_btn)

        next_btn = QAction(QIcon(os.path.join(web_browser_path, 'images', 'arrow-000.png')), "Forward", self)
        next_btn.setStatusTip("Forward to next page")
        next_btn.triggered.connect(lambda: self.tabs.currentWidget().forward())
        navtb.addAction(next_btn)

        reload_btn = QAction(QIcon(os.path.join(web_browser_path, 'images', 'arrow-circle-315.png')), "Reload", self)
        reload_btn.setStatusTip("Reload page")
        reload_btn.triggered.connect(lambda: self.tabs.currentWidget().reload())
        navtb.addAction(reload_btn)

        home_btn = QAction(QIcon(os.path.join(web_browser_path, 'images', 'home.png')), "Home", self)
        home_btn.setStatusTip("Go home")
        home_btn.triggered.connect(self.navigate_home)
        navtb.addAction(home_btn)

        navtb.addSeparator()

        self.httpsicon = QLabel()  # Yes, really!
        self.httpsicon.setPixmap(QPixmap(os.path.join(web_browser_path, 'images', 'lock-nossl.png')))
        navtb.addWidget(self.httpsicon)

        self.urlbar = QLineEdit()
        self.urlbar.returnPressed.connect(self.navigate_to_url)
        self.urlbar.setFont(QFont("Arial", 10))
        navtb.addWidget(self.urlbar)

        add_bookmark_btn = QAction(QIcon(os.path.join(web_browser_path, 'images', 'star.png')), "Add a new bookmark", self)
        add_bookmark_btn.setStatusTip("Add a new bookmark")
        add_bookmark_btn.triggered.connect(self.add_bookmark)
        navtb.addAction(add_bookmark_btn)

        stop_btn = QAction(QIcon(os.path.join(web_browser_path, 'images', 'cross-circle.png')), "Stop", self)
        stop_btn.setStatusTip("Stop loading current page")
        stop_btn.triggered.connect(lambda: self.tabs.currentWidget().stop())
        navtb.addAction(stop_btn)

        menu = QMenu(self)

        new_tab_action = QAction(
            QIcon(os.path.join('images', 'ui-tab--plus.png')), "New Tab", self)
        new_tab_action.triggered.connect(lambda _: self.add_new_tab())
        menu.addAction(new_tab_action)

        settings_action = QAction(QIcon(os.path.join('images', 'wrench.png')), "Settings", self)
        settings_action.triggered.connect(show_settings)
        menu.addAction(settings_action)

        history_action = QAction("History", self)
        history_action.triggered.connect(self.history)
        menu.addAction(history_action)

        bookmarks_action = QAction("Bookmarks", self)
        bookmarks_action.triggered.connect(lambda _: self.add_new_tab(QUrl("web_browser://bookmarks")))
        menu.addAction(bookmarks_action)

        about_action = QAction(QIcon(os.path.join('images', 'question.png')), "About Web Browser", self)
        about_action.triggered.connect(self.about)
        menu.addAction(about_action)

        option_btn = QAction(
            QIcon(os.path.join(web_browser_path, 'images', 'blank.png')), "Option", self)
        option_btn.setMenu(menu)
        navtb.addAction(option_btn)

        self.menuBar().setNativeMenuBar(False)

        file_menu = self.menuBar().addMenu("&File")

        new_tab_action = QAction(QIcon(os.path.join(web_browser_path, 'images', 'ui-tab--plus.png')), "New Tab", self)
        new_tab_action.setStatusTip("Open a new tab")
        new_tab_action.triggered.connect(lambda _: self.add_new_tab())
        file_menu.addAction(new_tab_action)

        quit_action = QAction("Quit", self)
        quit_action.setStatusTip("Quit Web Browser")
        quit_action.triggered.connect(quit)
        file_menu.addAction(quit_action)

        bookmarks_menu = self.menuBar().addMenu("&Bookmarks")

        bookmarks_file = open(web_browser_path + 'bookmarks.txt', 'r')
        bookmarks = bookmarks_file.readlines()

        for bookmark in range(0, len(bookmarks)):
                locals()["bookmark_action_" + bookmarks[bookmark]] = QAction(bookmarks[bookmark], self)
                locals()["bookmark_action_" + bookmarks[bookmark]].setStatusTip("Open " + bookmarks[bookmark])
                locals()["bookmark_action_" + bookmarks[bookmark]].triggered.connect(partial(self.open_bookmark, bookmarks[bookmark]))
                bookmarks_menu.addAction(locals()["bookmark_action_" + bookmarks[bookmark]])

        bookmarks_file.close()

        help_menu = self.menuBar().addMenu("&Help")

        about_action = QAction(QIcon(os.path.join(web_browser_path, 'images', 'question.png')), "About Web Browser", self)
        about_action.setStatusTip("Find out more about Web Browser")
        about_action.triggered.connect(self.about)
        help_menu.addAction(about_action)

        self.printer = QPrinter()

        try:
            if validators.url(sys.argv[1]):
                self.add_new_tab(QUrl(sys.argv[1]), '')
            else:
                self.add_new_tab(QUrl(homepage), 'Homepage')
        except:
            self.add_new_tab(QUrl(homepage), 'Homepage')

        self.shortcut = QShortcut(QKeySequence("Ctrl+T"), self)
        self.shortcut.activated.connect(self.add_new_tab)

        self.shortcut = QShortcut(QKeySequence("Ctrl+R"), self)
        self.shortcut.activated.connect(lambda: self.tabs.currentWidget().reload())

        self.shortcut = QShortcut(QKeySequence("Ctrl+W"), self)
        self.shortcut.activated.connect(lambda: self.close_current_tab(self.tabs.currentIndex()))

        self.shortcut = QShortcut(QKeySequence("Ctrl+U"), self)
        self.shortcut.activated.connect(self.view_source)

        # opening window in maximized size
        self.showMaximized()

        self.show()

        self.setWindowTitle("Web Browser")

        self.setWindowIcon(QIcon(os.path.join(web_browser_path, 'images', 'web_browser-icon-64.png')))

        global jsEnabled
        jsEnabled = True
        self.tabs.currentWidget().page().settings().setAttribute(QWebEngineSettings.JavascriptEnabled, jsEnabled)

    def getBookmarks(self):
        bookmarks_file = open(web_browser_path + 'bookmarks.txt', 'r')
        bookmarks = bookmarks_file.readlines()

        bookmarks_file.close()

        return bookmarks

    def add_bookmark(self):
        currentTabUrl = self.tabs.currentWidget().url().toString()

        bookmarksFile = open(web_browser_path + 'bookmarks.txt', 'r')
        bookmarksFileLines = bookmarksFile.readlines()

        containsBookmark = False
        for bookmarksFileLine in bookmarksFileLines:
            if bookmarksFileLine.strip() == currentTabUrl.strip():
                containsBookmark = True
                break

        bookmarksFile.close()

        if not containsBookmark:
            with open(web_browser_path + 'bookmarks.txt', 'a') as file:
                file.write('\n' + currentTabUrl)

    def view_source(self):
        url = self.urlbar.text()
        if not url.startswith('view-source:'):
            self.add_new_tab(QUrl("view-source:" + url))

    def open_bookmark(self, bookmark):
        self.add_new_tab(QUrl(bookmark.strip()))

    @pyqtSlot("QWebEngineDownloadItem*")
    def on_downloadRequested(self, download):
        old_path = download.url().path()  # download.path()
        suffix = QFileInfo(old_path).suffix()
        path, _ = QFileDialog.getSaveFileName(
            self, "Save File", old_path, "*." + suffix
        )
        if path:
            download.setPath(path)
            download.accept()

    def add_new_tab(self, qurl=None, label="New Tab"):

        global profile_name

        sysargv1 = homepage

        if qurl is None:
            qurl = QUrl(homepage)

        browser = QWebEngineView()
        profile = QWebEngineProfile(profile_name, browser)
        interceptor = WebEngineUrlRequestInterceptor()
        profile.setUrlRequestInterceptor(interceptor)
        webpage = WebPage(browser)
        browser.setPage(webpage)
        QWebEngineProfile.defaultProfile().downloadRequested.connect(self.on_downloadRequested)
        browser.loadFinished.connect(self.onLoadFinished)
        browser.setUrl(qurl)
        newtabI = self.tabs.addTab(browser, label)
        newtab = self.tabs.widget(newtabI)

        self.tabs.setTabIcon(newtabI, browser.page().icon())

        url = qurl.toString()
        if qurl.scheme() == "web_browser":
            html = ""
            if url.split("/")[2] == "about":
                with open(web_browser_path + "html/about.html", 'r') as f:
                    html = f.read()
            elif url.split("/")[2] == "newtab":
                with open(web_browser_path + "html/newtab.html", 'r') as f:
                    html = f.read()
            elif url.split("/")[2] == "welcome":
                with open(web_browser_path + "html/welcome.html", 'r') as f:
                    html = f.read()
            elif url.split("/")[2] == "history":
                html += "<title>History</title>"
                for j in self.tabs.currentWidget().page().history().backItems(100):
                    html = "<br>" + html
                    html = "<p><strong>" + j.title() + "</strong>" + "    <span style='color: grey;'>" + j.url().toString() + "</span></p>" + html
                html = "<br>" + html
                html = "<p><strong>" + self.tabs.currentWidget().page().title() + "</strong>" + "    <span style='color: grey;'>" + self.tabs.currentWidget().page().url().toString() + "</span></p>" + html
            elif url.split("/")[2] == "bookmarks":
                html += "<title>Bookmarks</title>"
                for j in self.getBookmarks():
                    html += "<a href='" + j + "'>" + j + "</a><br>"

            newtab.setHtml(html)
        else:
            # More difficult! We only want to update the url when it's from the
            # correct tab
            browser.urlChanged.connect(lambda qurl, browser=browser:
                                    self.update_urlbar(qurl, browser))

        browser.loadFinished.connect(lambda _, i=newtabI, browser=browser:
                                    self.setWindowTitle(self.tabs.currentWidget().page().title() + " - " + profile_name + " - Web Browser"))

        browser.loadFinished.connect(lambda _, i=newtabI, browser=browser:
                                    self.tabs.setTabText(newtabI, browser.page().title()[:30]))

        global jsEnabled
        browser.loadFinished.connect(lambda _, i=newtabI, browser=browser:
                                    self.tabs.currentWidget().page().settings().setAttribute(QWebEngineSettings.JavascriptEnabled, jsEnabled))

        browser.iconChanged.connect(lambda _, i=newtabI, browser=browser:
                                 self.tabs.setTabIcon(newtabI, browser.icon()))

        self.tabs.setCurrentIndex(newtabI)

    def onLoadFinished(self, ok):
        if is_64bits:
            bits = "64-bit"
        else:
            bits = "32-bit"
        if ok:
            if self.tabs.currentWidget().page().url().toString() == "file:///html/about.html":
                self.tabs.currentWidget().page().runJavaScript("document.getElementById('web_browser_version').innerHTML = 'Version " + version + " (" + bits + ")';")

    def tab_open_doubleclick(self, i):
        if i == -1:  # No tab under the click
            self.add_new_tab()

    def current_tab_changed(self, i):
        qurl = self.tabs.currentWidget().url()
        self.update_urlbar(qurl, self.tabs.currentWidget())
        self.update_title(self.tabs.currentWidget())

    def close_current_tab(self, i):
        if self.tabs.count() < 2:
            return

        self.tabs.removeTab(i)

    def update_title(self, browser):
        if browser != self.tabs.currentWidget():
            # If this signal is not from the current tab, ignore
            return

        title = self.tabs.currentWidget().page().title()
        self.setWindowTitle("%s - Web Browser" % title)

    def about(self):
        self.add_new_tab(QUrl("file:///html/about.html"), "About Web Browser")

    def history(self):
        self.add_new_tab(QUrl("web_browser://history"), "History")

    def navigate_home(self):
        self.tabs.currentWidget().setUrl(QUrl(homepage))

    def navigate_to_url(self, url=None):
        if url is None:
            url = self.urlbar.text()

        q = QUrl(url)
        if q.scheme() == "":
            if validators.url("http://" + url):
                q = QUrl("http://" + url)
            else:
                q = QUrl(search_engine_url.replace("%s", url))

        self.tabs.currentWidget().setUrl(q)

    def update_urlbar(self, q, browser=None):

        if browser != self.tabs.currentWidget():
            # If this signal is not from the current tab, ignore
            return

        if q.scheme() == 'https':
            # Secure padlock icon
            self.httpsicon.setPixmap(QPixmap(os.path.join(web_browser_path, 'images', 'lock-ssl.png')))

        else:
            # Insecure padlock icon
            self.httpsicon.setPixmap(QPixmap(os.path.join(web_browser_path, 'images', 'lock-nossl.png')))

        self.urlbar.setText(q.toString())
        self.urlbar.setCursorPosition(0)

    def toggle_javascript(self):
        global jsEnabled
        jsEnabled = not jsEnabled
        self.tabs.currentWidget().page().settings().setAttribute(QWebEngineSettings.JavascriptEnabled, jsEnabled)

class WebEngineUrlRequestInterceptor(QtWebEngineCore.QWebEngineUrlRequestInterceptor):
    def interceptRequest(self, info):
        url = info.requestUrl().toString()

class WebPage(QWebEnginePage):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.featurePermissionRequested.connect(
            self.handleFeaturePermissionRequested)

    def javaScriptConsoleMessage(self, level, message, lineNumber, sourceID):
        print("javaScriptConsoleMessage: ", message, "   ", sourceID)

    @pyqtSlot(QUrl, QWebEnginePage.Feature)
    def handleFeaturePermissionRequested(self, securityOrigin, feature):
        title = "Permission Request"
        questionForFeature = {
            QWebEnginePage.Geolocation: "Allow {feature} to access your location information?",
            QWebEnginePage.MediaAudioCapture: "Allow {feature} to access your microphone?",
            QWebEnginePage.MediaVideoCapture: "Allow {feature} to access your webcam?",
            QWebEnginePage.MediaAudioVideoCapture: "Allow {feature} to access your webcam and microphone?",
            QWebEnginePage.DesktopVideoCapture: "Allow {feature} to capture video of your desktop?",
            QWebEnginePage.DesktopAudioVideoCapture: "Allow {feature} to capture audio and video of your desktop?"
        }
        question = questionForFeature.get(feature)
        if question:
            question = question.format(feature=securityOrigin.host())
            if QMessageBox.question(self.view().window(), title, question) == QMessageBox.Yes:
                self.setFeaturePermission(
                    securityOrigin, feature, QWebEnginePage.PermissionGrantedByUser)
            else:
                self.setFeaturePermission(
                    securityOrigin, feature, QWebEnginePage.PermissionDeniedByUser)

    def createWindow(self, type_):
        if not isinstance(window, MainWindow):
            return

        if type_ == QWebEnginePage.WebBrowserTab:
            return window.add_new_tab(QUrl("file:///html/feature_unavailable.html"))

app = QApplication(sys.argv)
app.setApplicationName("Web Browser")
app.setOrganizationName("SimpleApps")

web_browser_path = str(pathlib.Path(__file__).parent.resolve()) + "/"

# Think forward not backwards
web_browser_path = web_browser_path.replace("\\", "/")

with open(web_browser_path + 'config.toml', 'r') as f:
    config = toml.loads(f.read())

if config['homepage'] == "default":
    homepage = "file:///html/newtab.html"
else:
    homepage = config['homepage']

if config['search_engine_url'] == "default":
    search_engine_url = "https://duckduckgo.com/?q=%s"
else:
    search_engine_url = config['search_engine_url']

window = MainWindow()
app.exec_()
